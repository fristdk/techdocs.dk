
# A computer in a flight case project
___

# Background

Why you might ask, well sometimes why not is enough for me. I often find my self teaching a class in basic network technology and suddenly I’m in desperate need of a couple of client and servers to demonstrate everything from a simple ping command and an arp request to more complicated [packet (and frame) inspection](https://en.wikipedia.org/wiki/Deep_packet_inspection) using [TCPDUMP](https://www.tcpdump.org/) and/or [Wireshark](https://www.wireshark.org/).


In classes, we sometimes set up remote detached networks and isolate it from the rest of the school network. Mostly to be on the safe side so that we do not interrupt the regular network traffic that other students are relying on. But also to get full control over the network and practice setting up a Lab network.
  

!!! tip "A sidetrack"
    This of cause brings up another good question, why do we (you) need a Lab network to practice on?
    This question I will try to answer in another post,
    click on “Why do you need a Lab network?” and
    read more about my personal take and
    recommendations.


Back to the track of this project. Because we segment the network at school, I do not always have access to the main network and the servers that we use for other parts of the teaching. Therefore, why not have a (very) small form factor computer that could preferably host a couple of virtualized clients and or servers ready to connect to a remote network.
  

# Requirements

If you read the background section here above or you maybe know a bit about the different form factors of modern computer motherboards, you will already by now know that the size of this computer is of relevance.
