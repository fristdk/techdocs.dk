# Introduction
This webpage is made using the static site generator [MkDocs](https://mkdocs.org) It is specifically made for building in hosting
project documentation. All source files are written in Markdown and you are doing the configuration in a
[YAML](https://en.wikipedia.org/wiki/YAML) configuration file. There are two built-in themes,
but you can use [3rd party themes](https://github.com/mkdocs/mkdocs/wiki/MkDocs-Themes)  or design your own.

For this project, I’m using a theme called [Material](https://squidfunk.github.io/mkdocs-material/). The theme is built with inspiration from Google’s [Material Design](https://material.io/design/) guidelines.

The basic idea with this setup was to get knowledge of and working with a static site generator on a remote cloud Linux server so that I could work with the project from everywhere without bringing a specific laptop.

The setup is roughly like this:
![topology](img/techDocs_infrastructure.jpg)
*Above: A early-stage topology of the infrastructure of the system. Made in draw.io*

Here is a list of technologies involved in this little setup:

Name		    | Technology    		            | Comments
:-----------	| :------------ 		            | -----------:
Linux server	| Amazon Web services  	            | Free Tier with elastic IP
Git	     	    | GitLab account  		            | Privat account
Hosting	        | One.com			                | 
Editors	        | Nano on Server - gitlab WebIde	|
Terminal / FTP  | Putty / FileZilla                 |

!!! note
    I have an amazon web services account through work, 
    this means that I always have access to free tiers,
    that was the reason for choosing Amazon as the service provider for a cloud server.
    I could use any other provider. And I think I will try Google next.    


## Linux server
 Why use a cloud server? Developing and typing this web page could easily have been done and maintained using a regular laptop or stationary computer. But the idea of having the storage and access to the computer fra anywhere using a remote cloud server could be useful. I also needed a common place to keep information.



 
