# Introduktion til Python (DK)

## Hvorfor denne side?

Fordi jeg i mit virke som underviser på en mellemlang videregående uddannelse indenfor IT på Erhvervsakademi Aarhus har oplevet en støt stigning i tiltrækning mod et krydsfelt mellem softwareudvikling og hardwareudvikling. Det er i dette krydsfelt at jeg også ønsker at placere tingenes internet, Internet of Things, eller som vi fremover vil kalde det, IoT. Ingen tvivl om, at det er et emne som nævnes igen og igen. Vi behøver heller ikke gå så langt for at opdage at søgningen mod emnet har været i en udvikling siden 2013. (se figur nedenfor)Dette til trods for, at udtrykket stammer tilbage fra 1999.


!!! info
    Læs mere om internet of things her:
    Madakam S, Ramaswamy R, Tripathi S. Internet of Things (IoT): A Literature Review. J Comput Commun. 2015;03(05):164–73.

## Interesse for IoT og Python

<script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/2051_RC11/embed_loader.js"></script><script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"internet of things","geo":"","time":"2004-01-01 2019-12-18"}],"category":0,"property":""}, {"exploreQuery":"date=all&q=internet%20of%20things","guestPath":"https://trends.google.dk:443/trends/embed/"});</script>

*Ovenfor ses en graf over hypigheden af søgninger på "internet of things" foretaget i hele verden via Google.*

Hvis vi sammeligner den ovenstående graf med en lignende for søgninger foretaget på søgeordet "Python" giver det et lignende billede.

<script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/2051_RC11/embed_loader.js"></script>
<script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"/m/05z1_","geo":"","time":"2004-01-01 2019-12-20"}],"category":0,"property":""}, {"exploreQuery":"date=all&q=%2Fm%2F05z1_","guestPath":"https://trends.google.dk:443/trends/embed/"}); </script>

*Ovenfor ses en graf over hypigheden af søgninger på "Python" foretaget i hele verden via Google.*

Af de ovenstående to grafer kan vi se at interessen for programmeringssproget Python er voksende og søgningen mod programmeringssproget har nok aldrig været større end nu. Derimod kunne det se ud som om, at IoT har toppet og måske er ved at finde sin plads og anvendelse. 

## Hvad er Python?

Python er et fortolket platformsuafhængigt programmeringssprog. Hvad vil det så sige? Mange programmeringssprog som Java og C++ skal kompileres til maskinkode som computeren herefter udfører, men lige som med JavaScript er der en oversætter som oversætter (translatere) vores programmeringskode til kommandoer som udføres af det system/computer som vi har skrevet koden til.

I følge den globale organisation for udvikling indenfor teknology [IEEE](https://www.ieee.org/) er Python nummer et blandt programmeringssprog når det handler om job og interesse.

![IEEE top programming languages](/img/python_IEEE_top_languages.png)

*Ovenfor ses en oversigt over de mest populære programmeringssporg i verden, i en undersøgelse udført at organisationen IEEE eller I triple E. du kan læse mere om den her: [IEEE Spektrum](https://spectrum.ieee.org/static/interactive-the-top-programming-languages-2019)*


Du kan også læse mere om min baggrund og hvorfor netop denne side på siden [about](../about.md)
