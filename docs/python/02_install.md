# Installation af Python

## Baggrund

Lad os hoppe direkte ud i det, for at kunne skrive programmer i programmeringssproget Python skal vi først på en windows pc installere programmeringssproget. På nogle andre operativsystemer som MacOS og Linux er Python tit inkluderet. Vi kan igen installere sproget på mange måder, en måde er blandt andet at downloade det direkte fra python.org. Men heldigvis for os er der en god nem måde at gøre det på. Jeg vil i det følgende gå ud fra, at i sidder ved en pc med windows 10 installeret og nogenlunde opdateret.

## Thonny IDE

Vi vil hente og installere programmet Thonny. Thonny er et begynder IDE for Python. Hvad betyder nu IDE. Det står for Integrated Development Environment eller på dansk [integreret softwareudviklingsmiljø](https://da.wikipedia.org/wiki/Integreret_udviklingsmilj%C3%B8 "Wiki side om IDE"). Thonny er et rigtig godt sted at starte med programmering i Python. Det er Open-source og gratis. Programmet er udviklet på [Tarty Universitetet](https://www.google.com/maps/@58.3811285,26.7204161,3a,75y,265.43h,105.57t/data=!3m7!1e1!3m5!1sKHgp2NlOarYJgv5h6JgaJw!2e0!5s20180801T000000!7i13312!8i6656 "Google streetview af universitetet") i Estland.

Prøv i google at søge på “Thonny ide”, så vil du få følgende resultat.

![Google Search Thonny IDE](/img/python/thonny_01_google_search.png)

*Ovenfor ses et screenshot af en google søgning på "thonny ide".*

Vi starter vores installation med at besøge følgende hjemmeside [thonny.org/](https://thonny.ord "Hjemmeside for thonny IDE") I øverste venstre hjørne kan du se at vi kan hente en version både til Windows til Mac og til Linux, så skulle vi gerne være dækket nogenlunde ind. Så hent den version som passer til dit styresystem. Vi vil i første omgang vælge windows versionen.

![thonny IDE hjemmeside](/img/python/thonny_02_thonny_org_site.png)

*Ovenfor ses et screenshot fra thonny webpage og her kan vi se at programmet kan hentes til forskellige operativ systemer. Vi vil i første omgang hente den til Microsoft Windows.*

Klik på Windows versionen og du vil nu hente en executable fil, altså en fil som indeholder eksekverbar programkode læs mere om .exe filer på [Wikipedia](https://da.wikipedia.org/wiki/.exe "læs mere om .exe filer").

Filen som skal downloades fylder ikke mere end ca. 14Mb så det burde ikke tage lang tid. Så klik på "Save file"

![Thonny IDE save file](/img/python/thonny_03_save_file.png)


Når filen har downloadet, altså hentet til din computer vil du kunne dobbeltklikke på den for at eksekver det program den indeholder, altså udføre de kommandoer som er beskrevet i filen. I vores tilfælde er det en række kommandoer som viĺ installere Thonny IDE som samt Python 3 på vores computer. Når du har dobbeltklikket på filen skulle du gerne se følgende velkomst skærm.

![Thonny IDE installation](/img/python/thonny_04_welcome_screen.png)

*Ovenfor ses den velkomstskærm som du kan se når du skal installere Thonny IDE for første gang. Den vil gøre dig opmærksom på, at den vil installere programmet for den pågældende bruger som er logget ind. Men hvis du ønsker at installere det for alle brugere, altså hvis du har flere konti på din computer, skal du afslutte og afvikle installationen som Administrator. Dette vil vi i første omgang springe over og bare klikke på “Next”*

Her skal du bare klikke på next for at komme videre med installationen. Det næste skærmbillede som vi vil se giver os mulighed for at læse om bruger licensen osv. Vi kan se at vi kan benytte programmet helt frit (Free of charge). Så det er vi jo glade for og vi vil acceptere denne license ved at klikke på “I accept the agreement” og klikke “Next”. Se nedenstående billede. 

![Thonny IDE License Agreement](/img/python/thonny_05_license_agreement.png)

Vi skal nu vælge hvilken destination vi vil have programmet til at installere sig i, her gør vi klogt i blot at vælge den plads som programmet foreslår, så har kan vi bare klikke “Next” vær dog opmærksom på, at den fortæller at der skal være et bestemt antal megabyte (MB) minimum til rådighed. I mit tilfælde er det 85,4 MB der skal være til rådighed.

![Thonny IDE License Agreement](/img/python/thonny_06_select_destination.png)

Sidste valgmulighed inden vi kan begynde installationsprocessen giver os muligheden for at oprette et icon for programmet på vores desktop. Hvis vi vinker denne mulighed af, vil vi kunne starte programmet fra skrivebordet (desktop).

![Thonny IDE License Agreement](/img/python/thonny_07_create_desktop_icon.png)

Herefter følger en kort opsummering, og programmet gør opmærksom på, at vi nu er klar til at begynde installationen. Klik På “Install” for at påbegynde installationen.

![Thonny IDE License Agreement](/img/python/thonny_08_ready_to_install.png)

Nu vil installationsprogrammet pakke en masse filer ud og gøre klar til at vi kan bruge programmet og snart skrive Vores første kode. Men først skal alt gøre klart og det kan tage lidt tid afhængigt af hvilken computer du har.

![Thonny IDE License Agreement](/img/python/thonny_09_installing.png)

Når installationen forhåbentlig er veloverstået og alt er som det skal være, vil vi kunne se følgende skærmbillede som siger at det er en Great Success..

![Thonny IDE License Agreement](/img/python/thonny_10_great_success.png)






