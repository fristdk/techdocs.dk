# Malling Opfinder Laboratorium

Skørt navn, tænker du nok, men et eller skulle det jo hedde - og så passer det jo fint med, at [mol](https://da.wikipedia.org/wiki/Mol_(enhed)) er SI-enhed for stofmængde.

![mol logo](../img/corona/mol_logo.jpg)

----
## tilmed canvas rum

Du skulle gerne have modtaget en mail fra mig, heri er der et link til et virtuelt klasserum. Du vil blive bedt om at oprette en ny konto hos noget som hedder Canvas.

![opret canvas](../img/corona/tilmeld_canvas_MOL_01.jpg)

Her er det selvfølgelig vigtigt at du skriver din mail korrekt, du vil nemmelig modtage en mail hvor du skal bekræfte at det er din mail og følge et link til oprettelse af et kodeord.
Så husk:

* korrekt e-mail
* sæt kryds ved "jeg er en ny bruger"
* skriv dit fulde navn
* sæt kryds i "jeg er enig..."
* klik på "meld dig til faget"

Nu skulle du gerne komme til en skærm som vil se sådan her ud:

![canvas fag](../img/corona/tilmeld_canvas_MOL_02.jpg)

Her skal du blot klikke på "Gå til fag"

Nu skal du så huske at verificere din e-mail, klik på det link som er kommet i en mail fra Canvas, Den vil hedde noget med "Canvas Free for Teachers"

Så vil du se noget som ser cirka sådan her ud:

![canvas password](../img/corona/tilmeld_canvas_MOL_email_01.jpg)

Her skal du huske at:

1. Lav et password du kan huske
2. Sætte den korrekt tidszone
3. Husk at fravælge hvis du ikke vil have nyheds e-mail fra canvas
4. klik på Registrer

![canvas password](../img/corona/tilmeld_canvas_MOL_email_02.jpg)










