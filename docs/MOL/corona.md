# Invitation til eksperimenter og leg med programmering

![me](../img/corona/me.jpg)

Hej

Mit navn er Hans Henrik Jeppesen, jeg er til daglig underviser på Erhvervsakademi Aarhus og underviser på [Kompetencecenter for IT og Softwareudvikling](https://www.eaaa.dk/videregaende-uddannelser/vaelg-efter-interesse/it-og-teknologi/), primært i Netværk- og Serverteknologi. Jeg har også kurser i Internet of things og en lang række andre tekniske fag på akademi- og bachelorniveau. Som rigtig mange andre arbejder jeg i denne tid hjemme, og forsøger at holde mine studerende studieaktive og nysgerrige. Jeg bor med min familie i Malling og kan mærke den store opbakning til vores lokalsamfund og en sikring af, at alt kan genvinde det normale når vi er på den anden side af denne alvorlige krise. [mere om min baggrund](../about.md)

----

Derfor, en invitation til eksperimenter og leg. Jeg ved at mange skolebørn på mellemtrinnet har fået udleveret en micro:bit og de bliver sikkert flittigt brugt rundt omkring i disse dage. Det er jo bare fedt.

Jeg vil gerne tilbyde  en mulighed for, at de unge som går i udskolingen, måske på 1. eller 2. år af gymnasiet og påtænker at påbegynde en teknisk uddannelse i fremtiden, enten hos [Erhvervsakademi Aarhus](https://www.eaaa.dk), [Aarhus Universitet](https://au.dk) eller en af de mange andre muligheder som findes, kan samles (hver for sig) om at lære en ny teknologi og eksperimenter med programmering og IT.

![trinketM0](../img/corona/trinket_400X400.jpg)

Jeg har pt. 10 microcontroller af typen [Adafruit Trinket M0](https://www.adafruit.com/product/3500) hjemme i Malling. Disse vil jeg meget gerne låne ud til unge som har en interesse for IT og teknologi generelt. Du behøver ikke kunne programmere eller vide forskel på public og privat IP adresser. Så længe du er interesseret og har lyst til at lege og arbejde med teknologien. Jeg vil forsøge at oprette et virtuelt klasserum hvor jeg vil dele nogle tutorials, opgaver osv. Det vil være en fordel hvis du har en PC med opdateret windows 10, en Apple Mac med opdateret macOS, eller en Linux distribution. Jeg har ikke erfaring med Chrome Book’s. Du skal ligeledes bruge et [USB-A (2.0)](https://en.wikipedia.org/wiki/USB) til USB-micro-b data kabel.

Jeg tænker vi skal kigge på følgende emner:

* Hvad er og kan en microcontroller?
* introduktion til programmering
    * Python / Circuit-Python
* Netværksteknologi
    * Hvordan virker internettet
    * Hvad er en IP adresse og hvorfor er det nødvendigt?

Og helt sikkert mange flere spændende ting, hvis der er tid og interesse for det.

## Software / Hardware
Det er vigtigt at have noget hardware at øve sig på og jeg har 10 stk. Microcontroller jeg kan udlåne. Desværre kan jeg ikke afhjælpe tekniske udfordringer, som i en normal fysisk undervisningssession, så de som melder sig, skal være med på et eksperiment og løse mange udfordringer på egen hånd. Men når det så er sagt, så vil jeg gøre alt hvad jeg kan for at præsentere mit bud på en verden jeg finder utrolig inspirerende og sjov. 

Jeg tænker, at når 10 personer har tilmeldt sig via link du kan finde nederst på siden under *Sign-up*, så cykler jeg rundt med de enkelte microcontroller i jeres postkasser - microcontrollerne er stadig indpakket fra fabrikken og har været det siden jeg anskaffede dem for over et halv år siden. Hvis i afspritter og følger god håndhygiejne som foreskrevet af de danske myndigheder ved modtagelse, er det min bedste overbevisning at vi ikke opsætter en smittekæde.


----

![jef_logo](../img/corona/jef_logo.png)

Denne mulighed kunne ikke lade sig gøre uden støtte og opbakning fra vores lokale virksomhed i Malling, [**Jydsk Emblem Fabrik**](https://www.jef.dk) for som Administrerende Direktør Stig Hellstern siger _“Vi er utrolig afhængig af vores lokalmiljø og folk med tekniske kompetencer er vigtig for en virksomhed som vores og ikke mindst den verden vi lever i. Vi ønsker at støtte op om alt der samler Beder-Malling uden at være fysisk sammen i disse tider”._


----

## Sign-up
Hvis du har lyst til at lege med og eksperimentere skal du skrive dig op via følgende link

[LINK TIL GOOGLE FORM](https://forms.gle/irqM588MPWbQeZMJ6)
