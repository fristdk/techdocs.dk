# Welcome to techDocs.dk

Den korte forklaring er, at jeg manglede et hjem til et voksende bibliotek af how-to guides. Nogle på engelsk andre på dansk. Disse vil være at finde fra denne side. Håber de kan være til nytte for andre. I første omgang har dokumenterne tjent som en opskriftsbog til eget brug.

Hvis du opdager fejl og mangler er du meget velkommen til at kontakte mig på mail venligst se [About](about.md) for detaljer.

-----
## Invitation til eksperimenter og leg med programmering
Du er sikkert kommet fra Facebook og bor i Beder-Malling
Se mere her [Corona](MOL/corona.md)
-----

## The English version

To keep a story short, I needed a place to put all of my personal how-to-do guides made so that I can redo and remember the things and settings I make on different systems. A lot is centred around the raspberry pi and cloud solutions all more or less related to the internet of things. In my work, I set up and construct a growing number of showcases and tutorials, some of these guides found on this page have served as a template for this work. Maybe it can be useful for other than me. Some of the notebooks will be in Danish and some in English, it depends a bit on which audience the tutorial or project was intended for.

Hope you will find this useful and if you spot any mistakes and or have suggestions for improvements. Please do contact me, see [About](about.md) for details. 
