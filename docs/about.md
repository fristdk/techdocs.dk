# About
Mit udgangspunkt for at lege og udforske teknologi og computerprogrammering starter for lang tid tilbage. Lige siden familiens første Commodore 64, er mange timer og nætter gået foran en skærm i et forsøg på lige at ændre på noget.
Efter nogle år på medicinstudiet i Odense, er det blevet til en uddannelse som Cand. Scient i Klinisk Videnskab og Teknologi. Jeg har været forskningsassistent på Regionshospital Nordjylland - Hjørring hvor jeg blandt andet har været med til
at undersøge misbrugsproblemer og udviklet nye og bedre metoder til diagnosticering. Til dagligt er jeg ansat som adjunkt og underviser på Erhvervsakademi Aarhus under kompetencecenter for IT og Softwareudvikling.

##Commodore 64 og velfærd
Men før jeg påbegyndte mit nuværende job som underviser studerede jeg Klinisk Videnskab og Teknologi på Aalborg Universitet. Her blev vi som studerende udfordret af de mange forskellige teknologier som i disse år har sit indtog inden for velfærdsområdet. Jeg fandt det specielt interessant at se hvordan teknologier bliver implementeret, hvilke muligheder og til tider modstande der opstår når mennesker og teknologier skal forsøge at finde samspil.
###Micro:bit og Studiejob
Jeg var så heldig under mit studie at få et studiejob ved Skanderborg Kommunale Ungdomsskole. De ønskede at kunne tilbyde programmering og dermed højne IT kendskab til unge fra 7. klasse. En opgave og udfordring som lød alt for spændende og jeg hoppede straks ombord. Nu skulle jeg så bare finde ud af hvordan. Jeg ønskede at finde en fysisk repræsentation af den kode som skulle skrives, altså en større fysisk oplevelse end da jeg første gang afprøvede programmering.
###1984 - C64
Jeg var så heldig at min far var villig til at investere i en computer da jeg var 9 år. Så omkring 1984 købte han og jeg en Commodore 64 som bleve starten på min interesse for computer og programmering. På den fantastiske maskine forsøgte jeg at lære at programmere i et sprog som hedder BASIC. Den ikoniske blå skærm som efter tilkobling til vores TV skrev: READY er nok kendt af mange, se billeder herunder. Dog var min første skærm ikke blå, men grå, jeg havde fået lov at låne vores 13” sort/hvide TV som ellers blev medbragt på diverse campingferier i Danmark.

![c64_screenshot](img/c64_ready.png)

*Ovenfor ses et screenshot fra en klar Commodore64 computer - Mange som var børn/unge i 1980’erne havde mulighed for at stifte bekendtskab med denne skærm. Den bragte mange lange aftener og nætter med sig når dens 8-bits mikroprocessor skulle programmeres.*

Har du ikke lige en Commodore64 ved hånden, kan du afprøve den eminente maskine via [virtualconsoles.com](https://virtualconsoles.com/online-emulators/c64/ "Virtuel Commodore64 emulator") 

###Hej Verden - Hello World

Jeg vidste selvfølgelig ikke dengang, at der er tradition for, at det første computerprogram man skriver i et nyt programmeringssprog kaldes for et “Hello world!” program, hej verden! Havde jeg vidst det, vil det sikkert kunne se sådan her ud:

```basic
10 PRINT “HEJ VERDEN”
```

Jeg kunne så herefter give kommandoen “RUN” og så ville mit lille program blive udført af computeren. Jeg synes du skal prøve at lave dit (måske) første Hej Verden program på en Commodore64. Hvis ikke du allrede har gjort det, prøv at åbne følgende side i en ny tab i i din browser [virtualconsoles.com](https://virtualconsoles.com/online-emulators/c64/ "Virtuel Commodore64 emulator")

Herefter prøv at skrive følgende program og tryk på "Enter"

```basic
10 PRINT “HEJ VERDEN”
RUN
```
Det skulle gerne se nogenlunde sådan her ud:

![c64_screenshot](img/c64_hej_verden_run.png)

*Commodore64 afvikler et simpelt “Hello, World!” program, og beviser dermed at syntaksen, altså den måde man skriver programmet på er korrekt og computeren har forbindelse til en ekstern enhed, i dette tilfælde TV skærmen.*

###Forudsætninger
Mange af de danske dokumenter på denne side er skrevet for at alle kan være med, derfor er det ikke forventet at du har skrevet kode og programmeret før. Det er ikke en forudsætning, men jeg vil anbefale at du gennemgår bogen sammen med en god ven/veninde som deler din interesse eller en forældre som ønsker at støtte op om og involvere sig i udviklingen af tekniske og teknologiske kompetencer hos børn og unge.

Der er et stort ønske om at, at mine opdagelser netop kan bruges som et samlingspunkt for nye opdagelser og udforskninger i den teknologi som vi omgiver os med.

Har du fået lyst til mere programmering og sjov med en computer så kig [Introduktionen til Python](python/01_intro.md "En introduktion til Python") 

Hans Henrik Jeppesen 2019
 
##Contact

Frist.dk  
Hans Henrik Jeppesen  
Sofienlystvej 9  
8340 Malling  
info@techdocs.dk

